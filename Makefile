# Makefile for the NSLU2 Linux development system
# Licensed under the GPL v2 or later

HOST_MACHINE:=$(shell uname -m | sed \
	-e 's/i[3-9]86/i386/' \
	-e 's/armv5tel/arm/' \
	-e 's/armv5teb/armeb/' \
	-e 's/armv5b/armeb/' \
	)

BITBAKE_BRANCH = 1.10
BITBAKE_VERSION = 1.10.2

.PHONY: all
all: update build

.PHONY: prefetch
prefetch: prefetch-slugos prefetch-optware

.PHONY: build
build:
	[ ! -e slugos ]   || ${MAKE} slugosbe slugosle
	[ ! -e optware ]  || ${MAKE} optware

.PHONY: setup
ifneq ($(HOST_MACHINE),armeb)
setup: setup-common setup-bitbake setup-openembedded \
       setup-slugos setup-optware
else
setup: setup-common setup-optware
endif

.PHONY: setup-developer
setup-developer: setup-common setup-bitbake setup-openembedded setup-optware-developer

.PHONY: update
ifneq ($(HOST_MACHINE),armeb)
update: update-common update-bitbake update-openembedded update-optware
else
update: update-common update-optware
endif

.PHONY: status
status: status-common status-bitbake status-openembedded status-optware

.PHONY: clobber
clobber: clobber-slugos \
	 clobber-optware

.PHONY: distclean
distclean: distclean-common distclean-bitbake distclean-openembedded \
	 distclean-slugos \
	 distclean-optware distclean-releases

.PHONY: prefetch-unslung
prefetch-unslung: unslung/.configured
	( cd unslung ; ${MAKE} prefetch )

.PHONY: prefetch-optware
prefetch-optware:
	[ ! -e optware ] || \
	for f in optware/platforms/packages-*.mk ; do \
	  export target=`echo $$f | sed -e 's|optware/platforms/packages-\(.*\)\.mk|\1|'` ; \
	  [ ! -e optware/$$target ] || make prefetch-optware-$$target ; \
	done

.PHONY: prefetch-optware-%
prefetch-optware-%: optware/%/.configured
	( cd optware/$* ; ${MAKE} source )

.PHONY: prefetch-unslung-%-beta
prefetch-unslung-%-beta: releases/unslung-%-beta/.configured
	( cd releases/unslung-$*-beta ; ${MAKE} prefetch )

.PHONY: prefetch-%
prefetch-%: %/.configured bitbake/.git/config openembedded/.git/config
	( cd $* ; ${MAKE} prefetch )

.PHONY: unslung unslung-image
unslung unslung-image: unslung/.configured
	( cd unslung ; \
	  ${MAKE} -k image )

.PHONY: slugosbe slugosbe-image
slugosbe slugosbe-image: slugos/.configured bitbake/.git/config openembedded/.git/config
	( cd slugos ; \
	  ${MAKE} setup-machine-nslu2be setup-distro-slugos setup-image-slugos-image ; \
	  ${MAKE} -k image )

.PHONY: slugosle slugosle-image
slugosle slugosle-image: slugos/.configured bitbake/.git/config openembedded/.git/config
	( cd slugos ; \
	  ${MAKE} setup-machine-nslu2le setup-distro-slugos setup-image-slugos-image ; \
	  ${MAKE} -k image )

.PHONY: openwrt openwrt-image
openwrt openwrt-image: openwrt/.svn/entries
	( cd openwrt ; ${MAKE} -k world)

.PHONY: unslung-packages
unslung-packages: unslung/.configured
	( cd unslung ; \
	  ${MAKE} -k distro )

.PHONY: slugosbe-packages
slugosbe-packages: slugos/.configured bitbake/.git/config openembedded/.git/config
	( cd slugos ; \
	  ${MAKE} setup-machine-nslu2be setup-distro-slugos setup-image-slugos-image setup-packages-slugos-packages ; \
	  ${MAKE} -k distro )

.PHONY: slugosle-packages
slugosle-packages: slugos/.configured bitbake/.git/config openembedded/.git/config
	( cd slugos ; \
	  ${MAKE} setup-machine-nslu2le setup-distro-slugos setup-image-slugos-image setup-packages-slugos-packages ; \
	  ${MAKE} -k distro )

.PHONY: openwrt-packages
openwrt-packages: openwrt/.svn/entries
	( cd openwrt ; ${MAKE} -k world )

.PHONY: %-packages
%-packages: %/.configured bitbake/.git/config openembedded/.git/config
	( cd $* ; ${MAKE} -k distro)

.PHONY: openwrt-index
openwrt-index: openwrt/.svn/entries
	( cd openwrt ; ${MAKE} -k package/index )

.PHONY: %-index
%-index: %/.configured bitbake/.git/config openembedded/.git/config
	( cd $* ; ${MAKE} -k index)

.PHONY: optware
optware:
	[ ! -e optware ] || \
	for f in optware/platforms/packages-*.mk ; do \
	  export target=`echo $$f | sed -e 's|optware/platforms/packages-\(.*\)\.mk|\1|'` ; \
	  [ ! -e optware/$$target ] || make optware-$$target ; \
	done

.PHONY: optware-%
optware-%: optware/%/.configured
	( cd optware/$* ; ${MAKE} autoclean ; ${MAKE} -k )

slugosle-%-beta: update-slugos-%-beta build-slugosle-%-beta
	echo "$@ completed"

slugosle-%-beta-image: update-slugos-%-beta build-slugosle-%-beta-image
	echo "$@ completed"

slugosbe-%-beta: update-slugos-%-beta build-slugosbe-%-beta
	echo "$@ completed"

slugosbe-%-beta-image: update-slugos-%-beta build-slugosbe-%-beta-image
	echo "$@ completed"

unslung-%-beta: update-unslung-%-beta build-unslung-%-beta
	echo "$@ completed"

openwrt-%: update-openwrt-% build-openwrt-%
	echo "$@ completed"

# The following two targets are deprecated,
# but are preserved for building old releases.

debianslug-%-beta: update-slugos-%-beta build-debianslug-%-beta
	echo "$@ completed"

openslug-%-beta: update-slugos-%-beta build-openslug-%-beta
	echo "$@ completed"

build-slugosle-%-beta: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; ${MAKE} -k slugosle-distro )

build-slugosle-%-beta-image: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; ${MAKE} -k slugosle-image )

build-slugosbe-%-beta: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; ${MAKE} -k slugosbe-distro )

build-slugosbe-%-beta-image: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; ${MAKE} -k slugosbe-image )

build-unslung-%-beta: releases/unslung-%-beta/.configured
	( cd releases/unslung-$*-beta ; ${MAKE} -k unslung-distro )

build-openwrt-%: releases/openwrt-%/.configured
	( cd releases/openwrt-$* ; ${MAKE} -k world package/index )

# The following two targets are deprecated,
# but are preserved for building old releases.

build-debianslug-%-beta: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; ${MAKE} -k debianslug-distro )

build-openslug-%-beta: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; ${MAKE} -k openslug-distro )

OE_SNAPSHOT_SITE := http://www.openembedded.org/snapshots

.PHONY: setup-common
.PRECIOUS: common/.git/config
setup-common common/.git/config:
	[ -e common/.git/config ] || \
	( git clone git://repo.or.cz/nslu2-linux/master.git common && \
	  rm -f Makefile && \
	  ln -s common/Makefile Makefile )
	touch common/.git/config

.PHONY: setup-bitbake
.PRECIOUS: bitbake/.git/config
setup-bitbake bitbake/.git/config:
	[ -d bitbake/.svn ] && \
	  mv bitbake bitbake_svn_obsolete
	[ -e bitbake/.git/config ] || \
	( git clone git://git.openembedded.org/bitbake bitbake ; \
	  cd bitbake && \
	  git checkout -b ${BITBAKE_BRANCH} --track origin/${BITBAKE_BRANCH} && \
	  git checkout ${BITBAKE_VERSION} )

.PHONY: setup-openembedded
.PRECIOUS: openembedded/.git/config
setup-openembedded openembedded/.git/config:
	[ -e openembedded/.git/config ] || \
	( git clone git://git.openembedded.org/openembedded openembedded ; \
	  cd openembedded ; \
	  git config --add remote.origin.fetch '+refs/heads/*:refs/remotes/*' )
	( cd openembedded && \
	  ( git branch | egrep -e ' org.openembedded.dev$$' > /dev/null || \
	    git checkout -b org.openembedded.dev --track origin/org.openembedded.dev ))
	( cd openembedded && git checkout org.openembedded.dev )
	touch openembedded/.git/config

.PHONY: setup-unslung
.PRECIOUS: unslung/.svn/entries
setup-unslung unslung/.svn/entries:
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e unslung/.svn/entries ] || \
	( svn co http://svn.nslu2-linux.org/svnroot/unslung/trunk unslung )
	touch unslung/.svn/entries

.PHONY: setup-unslung-%
setup-unslung-%: unslung/.svn/entries
	${MAKE} unslung/$*/.configured

.PHONY: setup-unslung-developer
setup-unslung-developer:
	[ ! -e unslung ] || ( mv unslung unslung-user )
	[ -e unslung/.svn/entries ] || \
	( svn co https://svn.nslu2-linux.org/svnroot/unslung/trunk unslung )
	${MAKE} setup-unslung

.PRECIOUS: unslung/.configured
unslung/.configured: common/.git/config unslung/.svn/entries
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e unslung/downloads ] || ( cd unslung ; ln -sf ../downloads . )
	rm -rf unslung/tmp/cache
	touch unslung/.configured

.PHONY: setup-optware
.PRECIOUS: optware/.svn/entries
setup-optware optware/.svn/entries:
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e optware/.svn/entries ] || \
	( svn co http://svn.nslu2-linux.org/svnroot/optware/trunk optware )
	touch optware/.svn/entries

.PHONY: setup-optware-%
setup-optware-%: optware/.svn/entries
	${MAKE} optware/$*/.configured

.PHONY: setup-optware-developer
setup-optware-developer:
	[ ! -e optware ] || ( mv optware optware-user )
	[ -e optware/.svn/entries ] || \
	( svn co https://svn.nslu2-linux.org/svnroot/optware/trunk optware )
	${MAKE} setup-optware

# This pattern rule has to come before the subsequent %/.configured openembedded pattern rule.
.PRECIOUS: optware/%/.configured
optware/%/.configured: optware/.svn/entries
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e optware/$*/Makefile ] || ( \
		mkdir -p optware/$* ; \
		echo "OPTWARE_TARGET=$*" > optware/$*/Makefile ; \
	 	echo "include ../Makefile" >> optware/$*/Makefile ; \
		ln -s ../../downloads optware/$*/downloads ; \
		ln -s ../make optware/$*/make ; \
		ln -s ../scripts optware/$*/scripts ; \
		ln -s ../sources optware/$*/sources ; \
	)
	touch optware/$*/.configured

.PHONY: setup-openwrt
.PRECIOUS: openwrt/.svn/entries
setup-openwrt openwrt/.svn/entries: common/.git/config
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e openwrt/Makefile ] || \
	( svn co https://svn.openwrt.org/openwrt/trunk openwrt )
	[ -e openwrt/dl ] || ( ln -s ../downloads openwrt/dl )
	[ -e openwrt/.config ] || ( cp common/conf/openwrt.config openwrt/.config )
	( cd openwrt ; \
	  ./scripts/feeds update ; \
	  ./scripts/feeds install -a ; \
	  ${MAKE} oldconfig )
	touch openwrt/.svn/entries

.PHONY: setup-openwrt-developer
setup-openwrt-developer:
	[ ! -e openwrt ] || ( mv openwrt openwrt-user )
	svn co https://${LOGNAME}@svn.openwrt.org/openwrt/trunk openwrt
	${MAKE} setup-openwrt

# This pattern rule has to come before the subsequent %/.configured openembedded pattern rule.
.PHONY: setup-openwrt-%
.PRECIOUS: releases/openwrt-%/.configured
setup-openwrt-% releases/openwrt-%/.configured:
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e releases/openwrt-$* ] || ( \
	  mkdir -p releases ; \
	  ( svn checkout https://svn.openwrt.org/openwrt/tags/$* \
	      releases/openwrt-$* | \
	    svn checkout https://svn.openwrt.org/openwrt/branches/$* \
	      releases/openwrt-$* ) \
	)
	[ -e releases/openwrt-$*/dl ] || ln -s ../../downloads releases/openwrt-$*/dl
	[ -e releases/openwrt-$*/.config ] || \
	( cp common/conf/openwrt-$*.config releases/openwrt-$*/.config )
	touch releases/openwrt-$*/.configured

.PHONY: setup-slugos
setup-slugos: setup-%:
	rm -rf $*/.configured
	${MAKE} $*/.configured

# This pattern rule has to come before the subsequent %/.configured openembedded pattern rule.
.PHONY: setup-unslung-%-beta
.PRECIOUS: releases/unslung-%-beta/.configured
setup-unslung-%-beta releases/unslung-%-beta/.configured:
	[ -e releases/unslung-$*-beta ] || ( \
	  mkdir -p releases ; \
	  svn checkout http://svn.nslu2-linux.org/svnroot/unslung/releases/unslung-$*-beta \
	    releases/unslung-$*-beta \
	)
	( cd releases/unslung-$*-beta ; ${MAKE} setup-env )
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e releases/unslung-$*-beta/downloads ] || \
	ln -s ../../downloads releases/unslung-$*-beta/
	touch releases/unslung-$*-beta/.configured

.PHONY: setup-unslung-%-beta-developer
setup-unslung-%-beta-developer:
	[ -e releases/unslung-$*-beta ] || ( \
	  mkdir -p releases ; \
	  svn checkout https://svn.nslu2-linux.org/svnroot/unslung/releases/unslung-$*-beta \
	    releases/unslung-$*-beta \
	)
	${MAKE} setup-unslung-$*-beta

# This pattern rule has to come before the subsequent %/.configured openembedded pattern rule.
.PHONY: setup-slugos-%-beta
.PRECIOUS: releases/slugos-%-beta/.configured
setup-slugos-%-beta releases/slugos-%-beta/.configured:
	[ -e releases/slugos-$*-beta ] || ( \
	  mkdir -p releases ; \
	  svn checkout http://svn.nslu2-linux.org/svnroot/slugos/releases/slugos-$*-beta \
	    releases/slugos-$*-beta \
	)
	( cd releases/slugos-$*-beta ; ${MAKE} setup-env )
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e releases/slugos-$*-beta/downloads ] || \
	ln -s ../../downloads releases/slugos-$*-beta/
	touch releases/slugos-$*-beta/.configured

.PHONY: setup-slugos-%-beta-developer
setup-slugos-%-beta-developer:
	[ -e releases/slugos-$*-beta ] || ( \
	  mkdir -p releases ; \
	  svn checkout https://svn.nslu2-linux.org/svnroot/slugos/releases/slugos-$*-beta \
	    releases/slugos-$*-beta \
	)
	${MAKE} setup-slugos-$*-beta

.PRECIOUS: %/.configured
%/.configured: common/.git/config
	[ -d $* ] || ( mkdir -p $* )
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e $*/Makefile ] || ( cd $* ; ln -sf ../common/openembedded.mk Makefile )
	[ -e $*/setup-env ] || ( cd $* ; ln -sf ../common/setup-env . )
	[ -e $*/downloads ] || ( cd $* ; ln -sf ../downloads . )
	[ -e $*/bitbake ] || ( cd $* ; ln -sf ../bitbake . )
	[ -e $*/openembedded ] || ( cd $* ; ln -sf ../openembedded . )
	[ -d $*/conf ] || ( mkdir -p $*/conf )
	[ -e $*/conf/local.conf.sample ] || \
	( cd $*/conf ; ln -sf ../../common/conf/local.conf.sample . )
	[ -e $*/conf/site.conf ] || ( cd $*/conf ; ln -sf ../../common/conf/site.conf . )
	[ -e $*/conf/auto.conf ] || ( \
		echo "DISTRO=\"$*\"" > $*/conf/auto.conf ; \
		echo "MACHINE=\"nslu2be\"" >> $*/conf/auto.conf ; \
		echo "IMAGE_TARGET=\"$*-image\"" >> $*/conf/auto.conf ; \
		echo "DISTRO_TARGET=\"$*-packages\"" >> $*/conf/auto.conf ; \
	)
	[ -e $*/conf/local.conf ] || ( cd $*/conf ; touch local.conf )
	rm -rf $*/tmp/cache
	touch $*/.configured

.PHONY: setup-host-debian
setup-host-debian:
	sudo apt-get install \
		autoconf automake1.9 \
		bison build-essential bzip2 \
		ccache cvs \
		devio diffstat \
		flex \
		gawk git-core \
		help2man \
		libncurses5-dev libtool \
		make \
		python-psyco \
		quilt \
		sed subversion \
		texi2html texinfo \
		unzip \
		xmlto \
		zlib1g-dev \

foo: \
		docbook \
		g++ gcj gfortran \
		libglib2.0-dev \
		m4 pkg-config python python-dev python2.4 python2.4-dev \
		sharutils

.PHONY: setup-host-ubuntu
setup-host-ubuntu:
	sudo apt-get install \
		autoconf automake automake1.9 \
		bison bzip2 \
		ccache cogito cvs \
		devio diffstat docbook \
		ed \
		fastjar flex \
		gcc gcc-3.3 g++ gawk gcj gettext gfortran git git-core groff-base \
		help2man \
		intltool \
		jikes \
		libc6-dev libglib2.0-dev libncurses5-dev libssl-dev libtool \
		libxml-parser-perl \
		m4 make \
		openssl \
		patch pkg-config python python-dev python2.4 python2.4-dev \
		quilt \
		rcs ruby \
		sdcc sed sharutils subversion sun-java5-jdk sysutils \
		tcl8.4 texinfo texi2html \
		unzip \
		zip zlib1g-dev
	@echo	
	@echo "To get python2.4-psyco (Recommended to speed up builds), please read"
	@echo "http://ubuntuguide.org/#extrarepositories" 
	@echo "NOTE: python2.4-psyco will not work on 64-bit archs"


.PHONY: setup-host-gentoo
setup-host-gentoo:
	su - -c " \
        emerge -n \
        autoconf automake \
        bison \
	ccache \
        cvs \
	flex \
	glib \
	libtool \
	m4 \
	make \
	patch \
	pkgconfig \
	sed \
	sharutils \
	sys-apps/texinfo \
	unzip \
	psyco \
	subversion \
	dos2unix \
	bzip2"

.PHONY: setup-host-centos
setup-host-centos:
	sudo yum install \
		bzip2 \
		cvs \
		diffstat \
		diffutils \
		gawk \
		gcc-c++ \
		git \
		gzip \
		makeinfo \
		patch \
		python-sqlite2 \
		quilt \
		rpm-build \
		subversion \
		tar \
		tetex \
		texi2html \
		texinfo
	sudo rpm -ivh http://www.python.org/pyvault/centos-4-i386/help2man-1.29-1.noarch.rpm

.PHONY: update-common
update-common: common/.git/config
	( cd common ; git pull )

.PHONY: update-bitbake
update-bitbake: bitbake/.git/config
	( cd bitbake && \
	  git checkout ${BITBAKE_BRANCH} && \
	  git pull && \
	  git checkout ${BITBAKE_VERSION} )

.PHONY: update-openembedded
update-openembedded: openembedded/.git/config
	( cd openembedded ; git pull )

.PHONY: update-unslung
update-unslung: unslung/.svn/entries
	( cd unslung ; svn update )

.PHONY: update-optware
update-optware: optware/.svn/entries
	( cd optware ; svn update )

.PHONY: update-openwrt
update-openwrt: openwrt/.svn/entries
	( cd openwrt ; svn update ; ./scripts/feeds update -a ; ./scripts/feeds install -a )

update-slugos-%-beta: releases/slugos-%-beta/.configured
	( cd releases/slugos-$*-beta ; svn update )

update-unslung-%-beta: releases/unslung-%-beta/.configured
	( cd releases/unslung-$*-beta ; svn update )

update-openwrt-%: releases/openwrt-%/.configured
	( cd releases/openwrt-$* ; svn update ; ./scripts/feeds update -a ; ./scripts/feeds install -a )

.PHONY: check-makefile
check-makefile:
	( wget -q -O - http://www.nslu2-linux.org/Makefile | diff -u Makefile - )

.PHONY: status-common
status-common: common/.git/config
	( cd common ; git diff --stat )

.PHONY: status-bitbake
status-bitbake: bitbake/.git/config
	( cd bitbake ; git diff --stat )

.PHONY: status-openembedded
status-openembedded: openembedded/.git/config
	( cd openembedded ; git diff --stat )

.PHONY: status-unslung
status-unslung: unslung/.svn/entries
	( cd unslung ; svn status )

.PHONY: status-optware
status-optware: optware/.svn/entries
	( cd optware ; svn status )

.PHONY: status-openwrt
status-openwrt: openwrt/.svn/entries
	( cd openwrt ; svn status )

status-slugosbe-%-beta: 
	( cd releases/slugosbe-$*-beta ; svn status )

status-unslung-%-beta: 
	( cd releases/unslung-$*-beta ; svn status )

.PHONY: clobber-unslung
clobber-unslung:
	[ ! -e unslung/Makefile ] || ( cd unslung ; ${MAKE} clobber )

.PHONY: clobber-slugos
clobber-slugos:
	[ ! -e slugos/Makefile ] || ( cd slugos ; ${MAKE} clobber )

.PHONY: clobber-openwrt
clobber-openwrt-%:
	[ ! -e openwrt/Makefile ] || ( cd openwrt ; ${MAKE} clean )

.PHONY: clobber-optware
clobber-optware:
	[ ! -e optware ] || \
	for f in optware/platforms/packages-*.mk ; do \
	  export target=`echo $$f | sed -e 's|optware/platforms/packages-\(.*\)\.mk|\1|'` ; \
	  [ ! -e optware/$$target ] || make clobber-optware-$$target ; \
	done

.PHONY: clobber-optware-%
clobber-optware-%:
	[ ! -e optware/$*/Makefile ] || ( cd optware/$* ; ${MAKE} distclean )

.PHONY: distclean-common
distclean-common:
	rm -rf common

.PHONY: distclean-bitbake
distclean-bitbake:
	rm -rf bitbake

.PHONY: distclean-openembedded
distclean-openembedded:
	rm -rf openembedded

.PHONY: distclean-unslung
distclean-unslung:
	rm -rf unslung

.PHONY: distclean-slugos
distclean-slugos:
	rm -rf slugos

.PHONY: distclean-openwrt
distclean-openwrt:
	rm -rf openwrt

.PHONY: distclean-optware
distclean-optware:
	rm -rf optware

.PHONY: distclean-releases
distclean-releases:
	rm -rf releases

.PHONY: distclean
distclean: distclean-common distclean-bitbake distclean-openembedded distclean-openwrt \
	   distclean-unslung distclean-slugos distclean-optware \
	   distclean-releases
	rm -rf downloads

# Targets for use by those with write access to the repositories

.PHONY: push
push: push-common push-openembedded

.PHONY: push-common
push-common: update-common
	( cd common ; git push ssh://repo.or.cz/srv/git/nslu2-linux/master.git )

.PHONY: push-openembedded
push-openembedded: update-openembedded
	( cd openembedded ; git push ssh://git@git.openembedded.org/openembedded )

# End of Makefile
